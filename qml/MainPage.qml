import QtQuick 2.0
import QtMultimedia 5
import QtQuick.Controls 2
Item {
    property int retrys
    property int whatx

    Audio {
        id: playMusic
        onStatusChanged: {
            if (playMusic.status == Audio.Buffered)
            {
                labelStatus.text="<u>Listen to:</u></center><center><b>"+channel+"</b>"
            }
        }

        onStopped:{
            //curSelButton.checked=false;
            if (channel!="" && retrys==100)
            {
                labelStatus.text="retry "+retrys+" times, gave up :-("
            }
            else if (channel!="") {
                labelStatus.text="retry in 2 seconds"
                retrys++
                timer.start()
            }
            else {
                /* user actually stoped player so we reset retry */
                retrys=0;
                labelStatus.text=""
            }
        }
        onError: labelStatus.text="broken pipe"


    }


    Timer
    {
        id: timer
        interval: 1000; running: false; repeat: false
        onTriggered:{
            playMusic.play()
        }
    }


    function f(err, str)
    {
       /* sometimes radio disconnect here we try again
            this is a bit quick&dirty we need to check better
            what type of connection etc... */
/*        if (err==NetworkError){
            playMusic.play();
        }
        */


    }

    function play(ch, url, b)
    {
        /* user actually changed channel using button, so retrys will reset */
        //vib.start()
        retrys=0
        channel=""
        playMusic.stop()
        labelStatus.text="please wait...\nBuffering..."
        playMusic.source=url
        channel=ch
        playMusic.play()
        curSelButton=b
    }

    Splash{
        id: splash
        anchors.centerIn: parent
    }

    Column {
        spacing: 12
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        Image{
            source: "qrc:/uradio_large.png"
            height: 200
            width: 200
            fillMode: Image.PreserveAspectFit
            anchors {
                horizontalCenter: parent.horizontalCenter
            }
            MouseArea{
                anchors.fill: parent
                onClicked: openFile("AboutPage.qml");
            }

        }

        Rectangle{
            gradient: Gradient {
                  GradientStop { position: 0.0; color: "#333333" }
                  GradientStop { position: 1.0; color: "#777777" }
              }
            anchors {
                horizontalCenter: parent.horizontalCenter
            }
            height: 3
            width: parent.width-64
        }

        Label {
            id: labelStatus
            color: "chocolate"
            horizontalAlignment: Text.AlignHCenter
            height: 80
            width: parent.width
            anchors {
                horizontalCenter: parent.horizontalCenter
            }
        }

        Button{
            anchors {
                horizontalCenter: parent.horizontalCenter
            }
            text: qsTr("Stop")
            onClicked:
            {
                channel=""
                //vib.start()
                playMusic.stop()
            }
        }
        Rectangle{
            gradient: Gradient {
                  GradientStop { position: 0.0; color: "#333333" }
                  GradientStop { position: 1.0; color: "#777777" }
              }
            anchors {
                horizontalCenter: parent.horizontalCenter
            }
            height: 3
            width: parent.width-64
        }

        Item {
            /* dumy item */
            height: 10
            width: parent.width
        }

        Repeater {
            model: uradio_model
            delegate: Button{
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                text: title
                onClicked: play(title, url, this)
            }
        }
    }

}
