// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 2.0
import QtQuick.Controls 2
ApplicationWindow {
    Column{
        anchors.fill: parent
        spacing: 6
        Slider{
            id: slide
            from: 0
            to: 19
            width:400
            value: 1
            onValueChanged: {
                theme.color = slide.value
            }

        }

        Button {
            text: "Done"
            onClicked: pageStack.pop();
        }
    }
}
