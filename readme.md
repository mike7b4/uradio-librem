# URadio for librem 5

Ported from sailfish. Using QML and Rust language.

## Dependies

  * QtMultimedia5
  * Qt Controls 2.0

## Rust dependies

  * qmetaobject
  * serde
  * toml
  * dirs

## Configuration

A default configuration file are put in ~/.config/uradio/config.toml

One can add a background image in ~/.config/uradio/background.jpg and this will be loaded by uradio when started.

## Todo

  - [ ] Edit radio channels.

