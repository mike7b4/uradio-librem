use std::collections::HashMap;
use std::fs::File;
use std::ffi::CStr;
use std::path::{PathBuf};
use std::fs;
use std::io::{Read, Write};
use qmetaobject::*;
use serde::{Serialize, Deserialize};
use toml;
use dirs;

#[derive(Deserialize, Serialize, Debug)]
struct Channel {
    title: String,
    url: String
}

impl Channel {
    fn new(title: &str, url: &str) -> Self {
        Self { title: title.to_string(), url: url.to_string() }
    }
}

#[derive(Deserialize, Serialize, Debug)]
struct Config {
    channels: Vec<Channel>,
}

#[derive(QObject, Default)]
struct UradioModel {
    base: qt_base_class!( trait QAbstractListModel ),
    count: qt_property!(i32; READ row_count NOTIFY count_changed),
    version: qt_property!(String; READ get_version),
    background: qt_property!(String; READ get_background),
    count_changed: qt_signal!(),
    list: Vec<Channel>,
//    get: qt_method!(fn(&self, index: i32) -> String),
    setup: qt_method!(fn(&mut self)),
    clear: qt_method!(fn(&mut self)),
}

impl UradioModel {
    fn get_version(&self) -> String {
        return String::from("0.1")
    }

    fn get_background(&self) -> String {
        let mut b = dirs::config_dir().unwrap();
        b.push("uradio/background.jpg");
        b.into_os_string().into_string().unwrap_or("".to_string())
    }

    fn setup(&mut self) {
        match self.load_config() {
            Ok(config) => {
                for item in config.channels {
                    self.push(item);
                }
            },
            Err(e) => {
                let mut config = Config{ channels: vec!() };
                let mut configpath = dirs::config_dir().unwrap();
                configpath.push("uradio/config.toml");
                eprintln!("Could not load config, fallback to defaults {:?}", e);
                config.channels.push(Channel::new("p1", "http://http-live.sr.se/p1-mp3-192")); 
                config.channels.push(Channel::new("p2", "http://http-live.sr.se/p2-mp3-192")); 
                config.channels.push(Channel::new("p3", "http://http-live.sr.se/p3-mp3-192")); 
                config.channels.push(Channel::new("p4 världen", "http://http-live.sr.se/srvarlden-mp3-192")); 
                match File::create(configpath) {
                    Ok(mut file) => {
                        let _ = file.write(&toml::to_string(&config).unwrap().into_bytes()).map_err(|_| 0);
                    },
                    Err(_) => {}
                }
                for item in config.channels {
                    self.push(item);
                }
             }
        };
    }

    fn push(&mut self, channel: Channel) {
        let end = self.list.len();
        (self as &mut QAbstractListModel).begin_insert_rows(end as i32, end as i32);
        self.list.insert(end, channel);
        (self as &mut QAbstractListModel).end_insert_rows();
        self.count_changed();
    }

    fn load_config(&self) -> Result<Config, std::io::Error> {
        let mut configpath = dirs::config_dir().unwrap();
        configpath.push("uradio/config.toml");
        let mut file = File::open(configpath)?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        match toml::from_str(&contents) {
            Ok(config) => {
                Ok(config)
            },
            Err(_) => {
                Err(std::io::Error::new(std::io::ErrorKind::InvalidInput, "parse error"))
            }
        }
    }

    fn clear(&mut self) {
        let len = self.list.len();
        (self as &mut QAbstractListModel).begin_remove_rows(0 as i32, len as i32);
        self.list.clear();
        (self as &mut QAbstractListModel).end_remove_rows();

        self.count_changed();
    }
}

impl QAbstractListModel for UradioModel {
    fn row_count(&self) -> i32 {
        self.list.len() as i32
    }

    fn data(&self, index: QModelIndex, role:i32) -> QVariant {
        let idx = index.row() as usize;
        if idx < self.list.len() {
            if role == USER_ROLE { QString::from(self.list[idx].title.clone()).into() }
            else if role == USER_ROLE + 1 { QString::from(self.list[idx].url.clone()).into() }
            else { QVariant::default() }
        } else {
            QVariant::default()
        }
    }

    fn role_names(&self) -> HashMap<i32, QByteArray> {
        let mut map = HashMap::new();
        map.insert(USER_ROLE, "title".into());
        map.insert(USER_ROLE + 1, "url".into());
        map
    }
}


qrc!(my_resource,
    "" {
        "qml/main.qml",
        "qml/MainPage.qml",
        "qml/Splash.qml",
        "uradio_large.png",
    },
);

fn main() -> Result<(), std::io::Error>{
    my_resource();
    qml_register_type::<UradioModel>(CStr::from_bytes_with_nul(b"UradioModel\0").unwrap(), 1, 0,
    CStr::from_bytes_with_nul(b"UradioModel\0").unwrap());
    let mut engine = QmlEngine::new();
    engine.load_file("qrc:///qml/main.qml".into());
    engine.exec();
    Ok(())
}
